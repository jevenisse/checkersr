﻿var checkersClient = angular.module('checkersClient', []);
var hub;
var proxy;
var movePos = [];
var myTurn = false;
var timesClicked;
movePos.length = 2;

checkersClient.controller('CheckersBoardCtrl', function ($scope) {
    $scope.showGrid = false;
    $scope.pawns = {};
    $scope.rows = [];
    $scope.legalMoves = [];
    $scope.rows.length = 10;
    $scope.player = { name: '', id: '', color: 'whitePawn' };
    $scope.foe = { name: '', id: '', color: 'blackPawn' };
    for(var i = 9;i>=0; i--)
    {
        $scope.rows[9 - i] = { id: i };
        $scope.rows[9 - i].columns = [];
        $scope.rows[9 - i].columns.length = 10;
        for (var j = 0; j < 10; j++) {
            $scope.rows[9 - i].columns[j] = { id: j };
        }
    }


    var processPawns = function (data) {
        $scope.pawns = {};
        angular.forEach(data,function(pawn,key) {
            if (pawn)
            {
                var playerColor = pawn.Color;
                if (pawn.IsQueen)
                    playerColor += "Queen";
                else
                    playerColor += "Pawn";
                $scope.pawns[pawn.X + ',' + pawn.Y] = { color: playerColor, isSelected: pawn.IsSelected };
            }

        });
    };
    var processMoves = function (data) {
        angular.forEach(data, function (move, key) {
            if (move) {
                $scope.legalMoves.push(move.X + ',' + move.Y);
            }
        });
    };
    $scope.move = function (x,y)
    {
        if (myTurn)
        {
            movePos[0] = x;
            movePos[1] = y;
            proxy.invoke('OnClick', movePos);
        }
    }
    $scope.joinGame = function () {
        if (!hub) {
            var queryString = "name=" + $scope.player.name;
            //instance a hub connection-->
            hub = $.hubConnection("http://localhost:8080");
            hub.qs = queryString;
            //instance a proxy from hub connection-->
            proxy = hub.createHubProxy('checkers');

            proxy.on('Refresh', function (result) {
                $scope.showGrid = true;
                $scope.legalMoves = [];
                myTurn = result[2] == $scope.player.id;
                processPawns(angular.fromJson(result[0]));
                processMoves(angular.fromJson(result[1]));
                $scope.$apply();
            });

            proxy.on('GameFound', function (result) {
                if (result[0] && result[0] != $scope.player.id)
                {
                    $scope.player.color = "blackPawn";
                    $scope.foe.color = "whitePawn";
                    $scope.foe.id = result[0];
                }
                if (result[1])
                    $scope.foe.name = result[1];
            });

            //start hub with success/fail hooks-->
            hub.start()
             .done(function () {
                 console.log('connection id: ' + hub.id);
                 $scope.player.id = hub.id;
                 proxy.invoke('FindGame');
             })
             .fail(function (failure) { /* hub start failure */ });
        }
    };
    $scope.checkFill = function (x, y)     {
        var result = (y.id % 2 != 0 && x.id % 2 == 0) || (y.id % 2 == 0 && x.id % 2 != 0) ? 'white' : 'black';
        if ($scope.legalMoves.indexOf(x.id + "," + y.id) >= 0 && myTurn)
            result += " legalMove";
        return result;
    }

    $scope.findPawnClass = function (x, y) {
        var pawn = $scope.pawns[x + ',' + y];
        if (pawn) {
            var pawnClass = pawn.color;
            if (pawn.isSelected && myTurn)
                pawnClass += " selected";
            return pawnClass;
        }
        return;
    };
});