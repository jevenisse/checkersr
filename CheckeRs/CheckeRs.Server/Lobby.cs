﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CheckeRs.Server
{
    [HubName("checkers")]
    public class Lobby : Hub
    {
        public override Task OnConnected()
        {
            var name = Context.QueryString["name"];
            var currentPlayer = new Player(name, Context.ConnectionId);

            if (!UserPool.AddPlayer(currentPlayer))
                return null;

            Console.WriteLine("{0} is connected", name);

            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            var removedPlayer = UserPool.RemovePlayer(Context.ConnectionId);
            if (removedPlayer!= null && !removedPlayer.IsPending)
            {
                GamePool.RemoveGame(removedPlayer.Game.GameId);

                Console.WriteLine("{0} is deconneted", removedPlayer.Name);
            }

            return base.OnDisconnected(stopCalled);
        }

        public  bool FindGame()
        {
            var currentPlayer = UserPool.GetPlayer(Context.ConnectionId);

            if (UserPool.PendingPlayersCount < 2)
                return false;

            var player = UserPool.GetFirstPendingPlayer(currentPlayer.Id);
            var game = new Game(player, currentPlayer);
            GamePool.AddGame(game);

            IClientProxy clientProxy = Clients.Client(player.Id);
            IClientProxy othClientProxy = Clients.Client(currentPlayer.Id);
            List<String> playerParams = (new String[] { game.PlayerTurnId, currentPlayer.Name }).ToList();
            List<String> currentPlayerParams = (new String[] { game.PlayerTurnId, player.Name }).ToList();

            var clientProxyTask = clientProxy.Invoke("GameFound", playerParams );
            var otherClientProxyTask = othClientProxy.Invoke("GameFound", currentPlayerParams);

            clientProxyTask.Wait();
            otherClientProxyTask.Wait();

            Refresh(game.GameId, null, endTurn:false);

            return true;
        }

        public bool OnClick(string[] coordinates)
        {
            int testIfIsANumber;
            if (!coordinates.All(s => int.TryParse(s, out testIfIsANumber)))
                return false;

            int[] intCoordinates = Array.ConvertAll(coordinates, int.Parse);

            var currentPlayer = UserPool.GetPlayer(Context.ConnectionId);
            var currentGame = GamePool.GetGame(currentPlayer.Game.GameId);

            if (currentGame.PlayerTurnId != Context.ConnectionId)
                return false;

            if (currentGame.GetSelectedPawn() == null)
                return PawnCicked(intCoordinates, currentPlayer, currentGame);

            return Moving(intCoordinates, currentGame);
        }

        private bool PawnCicked(int[] intCoordinates, Player currentPlayer, Game currentGame)
        {
            var currentPawn = currentGame.GetPawn(intCoordinates[0], intCoordinates[1]);

            if (currentPawn == null || currentPawn.OwnerId != currentPlayer.Id)
                return false;

            Task processing = currentGame.ProcessLegalsMoves(currentPawn);

            Refresh(currentGame.GameId, processing, endTurn : false);

            return true;
        }

        private bool Moving(int[] intCoordinates, Game currentGame)
        {
            bool success = false;
            var selectedPawn = currentGame.GetSelectedPawn();

            if (currentGame.LegalMoves.Contains(new Move(intCoordinates[0], intCoordinates[1])))
            {
                bool pawnIsEaten = currentGame.RemoveEatenPawn(selectedPawn, intCoordinates);
                selectedPawn.X = intCoordinates[0];
                selectedPawn.Y = intCoordinates[1];
                if (pawnIsEaten)
                {
                    currentGame.ProcessMoveToEat(selectedPawn);
                    if (currentGame.LegalMoves.Count > 0)
                    {
                        selectedPawn.IsLocked = true;
                        Refresh(currentGame.GameId, null, endTurn: false);
                        return true;
                    }
                    selectedPawn.IsLocked = false;
                }
                success = true;
            }

            selectedPawn.IsSelected = selectedPawn.IsLocked ? true : false;
            if(!selectedPawn.IsSelected)
                currentGame.EmptyLegalMoves();

            Refresh(currentGame.GameId, null, endTurn : success);

            CheckIsFinished(currentGame);

            return success;
        }

        private void CheckIsFinished(Game currentGame)
        {
            var winnerId = currentGame.WhoWin();
            if (winnerId != null)
            {
                IClientProxy clientProxys = Clients.Client(winnerId);

                clientProxys.Invoke("Finish", winnerId);
            }
        }

        private async void Refresh(object gameId, Task process, bool endTurn)
        {
            Game game = GamePool.GetGame((string)gameId);

            if (game == null)
                return;

            if(endTurn)
                game.PlayerTurnId = game.PlayersIds.Where(i => i != Context.ConnectionId).First();
        
            var json = game.GetPawnsListToJson();
            if(process != null)
                await process;
            var legalMoves = game.GetLegalMovesToJson();

            IClientProxy clientProxys = Clients.Clients(game.PlayersIds);
            List<String> results = new List<string>();
            results.Add(json);
            results.Add(legalMoves);
            results.Add(game.PlayerTurnId);
            await clientProxys.Invoke("Refresh", results);
        }
    }
}
