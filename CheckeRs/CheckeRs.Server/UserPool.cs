﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckeRs.Server
{
    public static class UserPool
    {
        private static ConcurrentDictionary<String, Player> _players = new ConcurrentDictionary<string, Player>();

        internal static bool AddPlayer(Player player)
        {
            return _players.TryAdd(player.Id, player);
        }

        internal static Player GetPlayer(string connectionId)
        {
            if (!_players.ContainsKey(connectionId))
                return null;

            Player player;

            _players.TryGetValue(connectionId, out player);

            return player;
        }

        internal static int PendingPlayersCount { get { return _players.Values.Where(p => p.IsPending).ToList().Count; } }

        internal static Player GetFirstPendingPlayer(string currentPlayerId)
        {
            if (_players.Values.Where(p => p.IsPending).ToList().Count < 2)
                throw new InvalidOperationException("There are no pending players");

            return _players.Where(p => p.Key != currentPlayerId).First().Value;
        }

        internal static Player RemovePlayer(String playerId)
        {
            Player playertToRemove = null;
            _players.TryRemove(playerId, out playertToRemove);

            return playertToRemove;
        }

        internal static bool ContainsPlayer(string id)
        {
            return _players.ContainsKey(id);
        }
    }
}
