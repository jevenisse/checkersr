﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckeRs.Server
{
    public class Move
    {
        private readonly int _x;
        private readonly int _y;
        public int X { get { return _x; } }

        public int Y { get { return _y; } }

        public Move(int x, int y)
        {
            _x = x;
            _y = y;
        }

        public override bool Equals(object obj)
        {
            var m = obj as Move;

            return m == null ? false :  m._x == _x && m._y == _y;
        }

        public override int GetHashCode()
        {
            return _x ^ _y;
        }
    }
}
