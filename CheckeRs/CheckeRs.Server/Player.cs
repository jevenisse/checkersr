﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckeRs.Server
{
    public class Player
    {
        private String _connectionId;
        private String _name;

        public Game Game { get; set; }

        public bool IsPending { get { return Game == null; } }
        public String Id
        {
            get
            {
                return _connectionId;
            }
        }

        public String Name
        {
            get
            {
                return _name;
            }
        }

        public Player(String name, String connectionId)
        {
            _connectionId = connectionId;
            _name = name;
        }



        
    }
}
