﻿using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace CheckeRs.Server
{
    public class Game
    {
        private const int boardSize = 10;

        private readonly String _gameId;
        private readonly Player _white;
        private readonly Player _black;

        private readonly IList<String> _playersIdList;

        private ConcurrentBag<Move> _legalMoves;

        private List<Pawn> _pawns;

        public String PlayerTurnId { get; set; }

        public String GameId
        {
            get { return _gameId; }
        }
        
        public IList<String> PlayersIds
        {
            get { return _playersIdList; }
        }

        public List<Move> LegalMoves
        {
            get { return _legalMoves.ToList(); }
        }

        public Game(Player white, Player black)
        {
            white.Game = this;
            black.Game = this;
            _white = white;
            _black = black;
            _pawns = new List<Pawn>();
            _gameId = white.Id + "+" + black.Id;
            _playersIdList = new String[] { white.Id, black.Id };
            PlayerTurnId = white.Id;

            InitializePawns();
        }



        public void PlayAgain()
        {
            InitializePawns();
        }

        private void InitializePawns()
        {
            for (int y = 0; y < 4; y++)
            {
                if (y % 2 == 0)
                {
                    for (int x = 0; x < boardSize; x = x + 2)
                    {
                        _pawns.Add(new Pawn(x, y, _white, "white"));
                        _pawns.Add(new Pawn(x, y + 6, _black, "black"));
                    }
                }
                else
                {
                    for (int x = 1; x < boardSize; x = x + 2)
                    {
                        _pawns.Add(new Pawn(x, y, _white, "white"));
                        _pawns.Add(new Pawn(x, y + 6, _black, "black"));
                    }
                }
            }
        }

        internal String GetPawnsListToJson()
        {
            String serializedResult = JsonConvert.SerializeObject(_pawns);

            return serializedResult;
        }

        internal String GetLegalMovesToJson()
        {
            if (_legalMoves == null)
                _legalMoves = new ConcurrentBag<Move>();

            String serializedLegalMoves = JsonConvert.SerializeObject(_legalMoves);

            return serializedLegalMoves;
        }

        internal Pawn GetPawn(int x, int y)
        {
            return _pawns.Where(p => p.X == x && p.Y == y).SingleOrDefault();
        }

        internal Pawn GetSelectedPawn()
        {
            return _pawns.Where(p => p.IsSelected).SingleOrDefault();
        }

        internal Task ProcessLegalsMoves(Pawn currentPawn)
        {
            return Task.Factory.StartNew(() =>
                {
                    if (_pawns.Where(p => p.IsSelected).SingleOrDefault() != null)
                        return;
                    currentPawn.IsSelected = true;
                    ProcessMoveToEat(currentPawn);

                    int maxDistanceX = currentPawn.Y + 2;
                    int maxDistanceY = currentPawn.X + 2;

                    if (_legalMoves.Count == 0)
                    {
                        if (currentPawn.IsQueen)
                        {
                            for (int i = -1; i < 2; i = i + 2)
                            {
                                for (int j = -1; j < 2; j = j + 2)
                                {
                                    for (int moveOne = 1; moveOne < boardSize; moveOne++)
                                    {
                                        if (_pawns.Where(p => p.Y == currentPawn.Y + i * moveOne && p.X == currentPawn.X + j * moveOne).Any())
                                            break;
                                        _legalMoves.Add(new Move(currentPawn.X + j * moveOne, currentPawn.Y + i * moveOne));
                                    }

                                }
                            }
                        }
                        else
                        {
                            for (int y = currentPawn.Y - 1; y < currentPawn.Y + 2; y = y + 2)
                            {
                                for (int x = currentPawn.X - 1; x < currentPawn.X + 2; x = x + 2)
                                {
                                    if (_pawns.Where(p => p.Y == y && p.X == x).Any())
                                        continue;

                                    if ((currentPawn.OwnerId == _white.Id && y == currentPawn.Y + 1 || currentPawn.OwnerId == _black.Id && y == currentPawn.Y - 1)
                                        && (x == currentPawn.X + 1 || x == currentPawn.X - 1))
                                        _legalMoves.Add(new Move(x, y));
                                }
                            }
                        }
                    }
                }
            );
        }

        internal void ProcessMoveToEat(Pawn currentPawn)
        {
            _legalMoves = new ConcurrentBag<Move>();

            for (int i = -1; i < 2; i = i + 2)
            {
                for (int j = -1; j < 2; j = j + 2)
                {
                    if (_pawns.Where(p => p.Y == currentPawn.Y + i && p.X == currentPawn.X + j && p.OwnerId != currentPawn.OwnerId).Any()
                        && !_pawns.Where(p => p.Y == currentPawn.Y + 2 * i && p.X == currentPawn.X + 2 * j).Any())
                        _legalMoves.Add(new Move(currentPawn.X + 2 * j, currentPawn.Y + 2 * i));
                }
            }
        }

        internal void EmptyLegalMoves()
        {
            _legalMoves = new ConcurrentBag<Move>();
        }

        internal bool RemoveEatenPawn(Pawn selectedPawn, int[] intCoordinates)
        {
            int eatenXPawn = selectedPawn.X + (intCoordinates[0] - selectedPawn.X) / 2;
            int eatenYPawn = selectedPawn.Y + (intCoordinates[1] - selectedPawn.Y) / 2;

            var eatenPawn = _pawns.Where(p => p.Y == eatenYPawn && p.X == eatenXPawn && p.OwnerId != selectedPawn.OwnerId).SingleOrDefault();

            if (eatenPawn != null)
            {
                _pawns.Remove(eatenPawn);
                return true;
            }

            return false;
        }

        internal void FreePlayers()
        {
            _white.Game = null;
            _black.Game = null;
        }

        internal string WhoWin()
        {
            if (!_pawns.Where(p => p.OwnerId == _white.Id).Any())
                return _black.Id;

            if (!_pawns.Where(p => p.OwnerId == _black.Id).Any())
                return _white.Id;

            return null;
        }
    }
}
