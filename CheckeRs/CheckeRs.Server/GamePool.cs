﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckeRs.Server
{
    public static class GamePool
    {
        private static ConcurrentDictionary<String, Game> _gamesInProgress = new ConcurrentDictionary<string, Game>();

        internal static bool AddGame(Game game)
        {
            return _gamesInProgress.TryAdd(game.GameId, game);
        }

        internal static Game GetGame(string gameId)
        {
            if (!_gamesInProgress.ContainsKey(gameId))
                return null;

            Game game;

            _gamesInProgress.TryGetValue(gameId, out game);

            return game;
        }

        internal static bool RemoveGame(string gameId)
        {
            Game removedGame;
            bool success =  _gamesInProgress.TryRemove(gameId, out removedGame);

            if(success)
                removedGame.FreePlayers();

            return success;
        }
    }
}
