﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CheckeRs.Server
{
    public class Pawn
    {
        private Player _owner;
        private String _color;
        private bool _isQueen;
        private int _y;

        public String OwnerId { get { return _owner.Id; } }
        public String Color { get { return _color; } }
        public int X { get; set; }
        public int Y 
        { 
            get
            {
                return _y;
            }
            set
            {
                _y = value;
                if (value == 9 && _color == "white" || value == 0 && _color == "black")
                    _isQueen = true;
            }
        }
        public bool IsQueen { get { return _isQueen; } }
        public bool IsSelected { get; set; }

        public bool IsLocked { get; set; }

        public Pawn(int x, int y, Player owner, String color)
        {
            X = x;
            Y = y;
            _owner = owner;
            _color = color;
        }
    }
}
