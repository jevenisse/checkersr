﻿using Microsoft.AspNet.SignalR.Client;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace CheckeRs.Client.WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        HubConnection _hubConnection;
        IHubProxy checkersHubProxy;

        private String myName;
        private ViewModel model;
        private bool playersTurn;
        public MainWindow()
        {
            InitializeComponent();
            model = new ViewModel();
            this.DataContext = model;
            initBoard();
        }

        private async void Send(object sender, RoutedEventArgs e)
        {
            if(_hubConnection !=null) return;

            myName = MsgToSend.Text;
            var querystringData = new Dictionary<string, string>();
            querystringData.Add("name", myName);
            
            _hubConnection = new HubConnection("http://localhost:8080", querystringData);
            checkersHubProxy = _hubConnection.CreateHubProxy("checkers");
            checkersHubProxy.On<List<String>>("Refresh", r => Refresh(r));
            checkersHubProxy.On<List<String>>("GameFound", r => GameFound(r));

            await _hubConnection.Start();

            bool b = await checkersHubProxy.Invoke<bool>("FindGame");
        }

        private void GameFound(List<String> results)
        {
            Dispatcher.BeginInvoke((Action)(() =>
            {
                MsgToSend.Visibility = System.Windows.Visibility.Collapsed;
                Sender.Visibility = System.Windows.Visibility.Collapsed;
                model.Me = new Player(myName, _hubConnection.ConnectionId, results[0] != _hubConnection.ConnectionId ? "black" : "white");
                model.Foe = new Player(results[1], null, results[0] != _hubConnection.ConnectionId ? "white" : "black");
                PlayerPawn.Source = (ImageSource)Resources[model.Me.Color + "Pawn"];
                FoePawn.Source = (ImageSource)Resources[model.Foe.Color + "Pawn"];
                //PlayerName.Text = myName;
                PlayerPawn.SetValue(Grid.RowProperty, 0);
                PlayerPawn.SetValue(Grid.ColumnProperty, 1);
                FoePawn.SetValue(Grid.ColumnProperty, 1);
                FoePawn.SetValue(Grid.RowProperty, 1);
                Game.Visibility = System.Windows.Visibility.Visible;
            }));
        }

        private void initBoard()
        {
            Rectangle cell;
            for (int i = 0; i < 10; i++ )
            {
                Board.ColumnDefinitions.Add(new ColumnDefinition());
                Board.RowDefinitions.Add(new RowDefinition());
            }

            for (int y = 9; y >= 0; y--)
            {
                for(int x = 0; x < 10;x++)
                {
                    cell = new Rectangle();
                    cell.Stretch = System.Windows.Media.Stretch.Fill;
                    cell.Uid = x + "," + y;
                    var color = (x%2!=0 && y%2==0) || (x%2==0 && y%2!=0) ?Colors.White: Colors.Black;
                    cell.Fill = new SolidColorBrush(color);
                    cell.MouseLeftButtonDown += OnClick;
                    cell.Height = 50.0;
                    cell.Width = 50.0;
                    cell.SetValue(Grid.ColumnProperty, x);
                    cell.SetValue(Grid.RowProperty, 9- y);
                    Board.Children.Add(cell);
                }
            }
        }

        private async void OnClick(object sender, MouseButtonEventArgs e)
        {
            Rectangle clickedCell = (Rectangle)sender;
            string[] coordinates = clickedCell.Uid.Split(',');
            bool b = await checkersHubProxy.Invoke<bool>("OnClick",coordinates.ToList());
        }

        protected override void OnClosed(EventArgs e)
        {
            _hubConnection.Dispose();

            base.OnClosed(e);
        }

        public void Refresh(List<String> positions)
        {
            Dispatcher.BeginInvoke((Action)(() =>
                {
                    RefreshInUI(positions);
                }));
        }

        private void RefreshInUI(List<String> positions)
        {
            reInitBoard();
            List<Pawn> pawns = JsonConvert.DeserializeObject<List<Pawn>>(positions[0]);
            List<Move> legalMoves = positions[1] != null ? JsonConvert.DeserializeObject<List<Move>>(positions[1]) : new List<Move>();
            playersTurn = positions[2].Equals(model.Me.Id);
            foreach(Rectangle child in Board.Children)
            {
                Pawn pawn = pawns.Where(p => child.Uid.Equals(p.X + "," + p.Y)).SingleOrDefault();
                if(playersTurn && legalMoves.Where(m => child.Uid.Equals(m.X + "," + m.Y)).Any())
                {
                    child.Fill = new SolidColorBrush(Colors.Azure);
                }
                else
                {
                    if (pawn != null)
                    {
                        if (!pawn.IsQueen)
                            child.Fill = new ImageBrush((ImageSource)Resources[pawn.Color + "Pawn"]);
                        else
                            child.Fill = new ImageBrush((ImageSource)Resources[pawn.Color + "Queen"]);
                    }
                }
            }
        }

        private void reInitBoard()
        {
            foreach(Rectangle cell in Board.Children)
            {
                String[] coords = cell.Uid.Split(',');
                int x ;
                int y;
                Int32.TryParse(coords[0], out x);
                Int32.TryParse(coords[1], out y);
                if ((y % 2 != 0 && x % 2 != 0) || (y % 2 == 0 && x % 2 == 0))
                    cell.Fill = new SolidColorBrush(Colors.Black);
            }
        }
    }
}
