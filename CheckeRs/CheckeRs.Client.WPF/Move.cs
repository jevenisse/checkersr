﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CheckeRs.Client.WPF
{
    class Move
    {
        private readonly int _x;
        private readonly int _y;
        public int X { get { return _x; } }

        public int Y { get { return _y; } }

        public Move(int x, int y)
        {
            _x = x;
            _y = y;
        }
    }
}
