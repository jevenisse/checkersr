﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CheckeRs.Client.WPF
{
    public class Player
    {
        private String _name;
        private string _playerId;
        private string _color;
        public String Id
        {
            get
            {
                return _playerId;
            }
        }
        public String Name
        {
            get
            {
                return _name;
            }
        }

        public String Color
        {
            get
            {
                return _color;
            }
        }
        public Player(String name, String playerId,String color)
        {
            _playerId = playerId;
            _name = name;
            _color = color;
        }
    }
}
