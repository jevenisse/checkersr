﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CheckeRs.Client.WPF
{
    class Pawn
    {
        private String _color;
        public String Color { get { return _color; } }
        public int X { get; set; }
        public int Y { get; set; }
        public bool IsQueen { get; set; }

        public Pawn(int x, int y, String color, bool isQueen)
        {
            X = x;
            Y = y;
            _color = color;
            IsQueen = isQueen;
        }
    }
}
