﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckeRs.Client.WPF
{
    public class ViewModel : INotifyPropertyChanged
    {
        private Player _foe;
        private Player _me;
        public Player Foe { get { return _foe; } set { _foe = value; FirePropertyChanged("Foe"); } }
        public Player Me { get { return _me; } set { _me = value; FirePropertyChanged("Me"); } }

        public event PropertyChangedEventHandler PropertyChanged;

        private void FirePropertyChanged(String propName)
        {
            if(PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }
    }
}
